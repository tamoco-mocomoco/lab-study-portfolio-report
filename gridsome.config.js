// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
module.exports = {
  siteName: 'Lab study portfolio report',
  plugins: [
    // Load all Blog Posts from file system
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/posts/**/*.md',
        typeName: 'Post',
        refs: {
          tags: {
            typeName: 'Tag',
            // typeName: 'AuthorTag',
            create: true,
          },
          author: {
            // typeName: 'Author',
            typeName: 'AuthorStudy'
          }
        },
      },
    },

    // Load Authors from file system
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/authors/**/*.md',
        typeName: 'Author',
        refs: {
          posts: {
            typeName: 'Post',
          },
          tags: {
            typeName: 'AuthorTag',
            create: true
          },
        },
      },
    },

    // Load Authors from file system
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/authors/**/*.md',
        typeName: 'AuthorStudy',
        refs: {
          posts: {
            typeName: 'Post',
          },
          tags: {
            typeName: 'AuthorTag'
          },
        },
      },
    },

    // Load About from file system
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/about/**/*.md',
        typeName: 'About',
        refs: {
          posts: {
            typeName: 'Post',
          },
        },
      },
    },
    // Netlify CMS Plugin
    {
      use: `gridsome-plugin-netlify-cms`,
      options: {
        publicPath: `/admin`,
      },
    },
  ],

  templates: {
    Post: [
      {
        path: '/blog/:year/:month/:day/:title',
        componenent: '~/templates/Post.vue',
      },
    ],
    Tag: [
      {
        path: '/tag/:title',
        componenent: '~/templates/Tag.vue',
        // componenent: '~/templates/AuthorTag.vue',
      },
    ],

    Author: [
      {
        path: '/author/:title',
        componenent: '~/templates/Author.vue',
      },
    ],
    AuthorStudy: [
      {
        path: '/author-study/:title',
        componenent: '~/templates/AuthorStudy.vue',
      },
    ],
    AuthorTag: [
      {
        path: '/author-tag/:title',
        componenent: '~/templates/AuthorTag.vue',
      },
    ],

  },

  transformers: {
    remark: {
      externalLinksTarget: '_blank',
      externalLinksRel: ['nofollow', 'noopener', 'noreferrer'],
      anchorClassName: 'icon icon-link',
      plugins: [
        // ...global plugins
      ],
    },
  },
};
