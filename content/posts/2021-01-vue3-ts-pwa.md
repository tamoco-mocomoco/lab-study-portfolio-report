---
title: 2021-01_vue3-ts-pwa
jp_title: Vue3 + TS + PWA
author: 000000000-lms
tags:
  - Frontend
  - Vue
  - TypeScript
  - Progressive-Web-App
participant:
  - 000000000-lms
  - 1000140174-tamoco
  - 1000140178-yasu
  - 1000170231-maehata
excerpt: Vue3とTypeScriptでPWAのアプリを作成します
date: 2021-01-23T10:00:00
featuredImage: uploads/vuejs-ts-pwa.png
---

# 概要
- Vue3でPWA(Progressve Web Apps)を作成します
- 言語はTypeScriptを選択し基礎から学習します
- PWAとはどういったモノなのかスマートフォンで閲覧したときの挙動も併せて説明します

# 環境構築
- VueCLIのインストールから開始しました
- VueCLIが上手くいかない環境が確認できました
- 一旦初期段階のコードをプルしてもらいローカルで動作するよう対応しました

# 基礎
- 初期のHello Worldの状態を少しずつカスタマイズ
- Vue3のHellow Worldの構成について説明します
- TypeScriptは既存のVueとの記述方法との違いについて説明しました

# スマートフォンによる実機確認

### PWAによってインストール

<img src="/vue3-ts-pwa/vue3-ts-pwa-3.png" width=50% alt="実機確認3">

<img src="/vue3-ts-pwa/vue3-ts-pwa-2.png" width=50% alt="実機確認2">

### インストール後にホーム画面に追加

<img src="/vue3-ts-pwa/vue3-ts-pwa-1.png" width=50% alt="実機確認1">

# 成果物

- 環境面に関する対応があったため成果物を作りこむ時間が少なくなりました
- PWAやTypeScriptの説明が多くなったため、まずは基礎的なアプリを完成させました

- [【カウントアプリ（Vue3+TS+PWA） 】](https://vue-counta.netlify.app/)
