---
title: 2020-11_python_tinker_gui
jp_title: PythonによるGUIアプリケーション
author: 1000200280-oono
tags:
  - Python
  - Tinker
  - SQLite

participant:
  - 1000200280-oono

excerpt: PythonによるGUIを使用した登録型辞書のアプリケーションです
date: 2020-11-01T10:00:00
featuredImage: uploads/abc-abl-screen2.png
---

# 1．概要
- 登録型辞書アプリ作成
- PythonによるGUIアプリケーション
- Tkinterを使用したフォームアプリケーション
- SQLiteにキーワードを登録
- 登録されたキーワードを検索

## 1-1．目的
- GUIアプリを作りたい
- データベースの学習も進めたい

## 1-2.GUI部分
- Pythonの標準モジュールであるTkinterを使用

## 1-3.データベース
- Pythonの標準モジュールであるsqlite3を使用

# 2.環境
- Windows10
- Python3.8.3
- Tkinter
- sqlite3

# 3.ソースコード
- [GitLab](https://gitlab.com/basuminton1365/abc-abl)にて公開しています

# 4.仕様
## 4-1.画面推移
- 登録画面・検索画面の2画面
- 上部ボタンで切り替えが可能

## 4-2.登録機能
- ユーザーが「言語」「種類」「内容」「説明」を入力し登録ボタンを押下する
  - 情報をデータベースに登録できる

## 4-3.検索機能
- 登録された内容の一覧を表示
- ユーザーが「キーワード」を入力し、検索ボタンを押すことにより部分一致で表示内容を絞る

## 4-4.その他
- 登録画面・検索画面はGUIによる直感的な操作が可能である
- 機能拡張に対応するため分かりやすい記述でコーディングする

# 5.スクリーンショット
## 5-1.登録画面

![登録画面](https://lab-study-portfolio-report.netlify.app/uploads/abc-abl-screen1.png)

## 5-2.検索画面
![検索画面](https://lab-study-portfolio-report.netlify.app/uploads/abc-abl-screen2.png)

# 6.まとめ
- 今回はオブジェクト指向を使わずに全て手続き型で記述した。
- 同じサイズやフォントのボタンを使いまわす場合はデフォルトのボタン型をクラス化したほうが記述がシンプルになる
- しかしこの規模でのクラス化はかえって分かりずらくなる
