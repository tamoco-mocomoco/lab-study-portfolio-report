---
title: 2021-04_lesson_ar_js
jp_title: Lesson AR.js
author: 000000000-lms
tags:
  - AR
  - JavaScript
  - Markdown

participant:
  - 000000000-lms
  - 1000140174-tamoco
  - 1000140178-yasu
  - 1000170231-maehata
  - 1000170156-yachi
  - 1000200280-oono
  - 1000210050-kobayashi
  - 1000210059-nakamura
  - 1000210065-kodera
  - 1000210072-aikawa
  - 1000210079-nakada

excerpt: AR.jsを使用してブラウザで楽しめるARサイトを作成します
date: 2021-04-24T10:00:00
featuredImage: uploads/lesson-ar-js.png
---

# 概要

- `AR.js`を使ってブラウザからカメラで AR のコンテンツを表示します
- 公式の Hello World から始まりカスタムのマーカーを使用するなど段階的につくっていきます
- このサイトでは段階的に学んだことを１つずつサイトとして作って記録しました
- 実際に作ったモノはスマホで AR を楽しむことができます


## 構成

- 静的なサイトで構成されています
- コンテンツの内容はマークダウンのテキストから読み込むようにしています
- それ以外はAR.jsのコンテンツごとにページを作っています
- Vue.jsなどのフレームワークを使用するとAR.jsの読み取れないときの原因特定が難しいためシンプルに作っています

<img width="70%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2Fdiagram.jpg?v=1618757359320">

## ソースコード

- glitch にて公開しています
- https://glitch.com/edit/#!/lesson-ar-js

## スマホで AR.js の AR を楽しむために・・・

### 準備

- まずはパソコンで開きながらスマホでもこのサイトを開いて準備します
- スマホで開く場合は以下の QRCode をご利用ください

<img width="35%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2FTopPage_QRCode.png?v=1618721508862">

### AR のサイトを開いたときの注意

- このサイトの AR のサイトを開く場合は Chrome の利用を推奨します
- カメラの許可を求められますので必ず許可してください

<div style="padding:2rem"></div>

# AR.js Lesson Start

## 01: Hello World

- スマホで特定のマーカーを写すと AR のコンテンツを表示するモノをつくります
- 公式の Marker Based Example を使ってみます
  - https://ar-js-org.github.io/AR.js-Docs/
  - Marker Based Example のソースコードを利用します
- パソコンで移した画像で AR を楽しむために`position`と`rotation`を以下のように設定します
  - position="0 0 1"
  - rotation="-90 0 0"

### ソースコード

[Glitch を開いてコードを確認できます (01-ar-hello-world.html)](https://glitch.com/edit/#!/lesson-ar-js?path=01-ar-hello-world.html)

### スマホでの動作

- [スマホでリンクを開いて AR の動作を確認できます (01-ar-hello-world.html)](https://lesson-ar-js.glitch.me/01-ar-hello-world.html)
- 以下の QRCode をスマホのカメラで写すと AR のオブジェクト（恐竜）が表示されます
- 3D モデルの読み込みが上手くいかない場合があるので表示されない場合は Lesson2 で確認してください

<img width="35%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2Fhiro.png?v=1618722159489">

<div>
  <img src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2F01-screen_shot-min.jpg?v=1618755326111">
</div>

## 02: 任意の 3D モデルファイルを読み込む

- Hello World では公式の恐竜の 3D モデルを表示していました
- 3D のモデルファイルを任意のモノを読み込ませ表示させてみます
- 使用する 3D モデルは以下を利用させていただきました
  - https://sketchfab.com/3d-models/flash-cube-5b5ffe7d529e43ebaa8980c0f36eca87
- 注意点としては 3D モデルが読み込めず表示できないことがあります
- 最初はモデルの問題かソース側の問題か切り分けが難しいので慣れてから 3D モデルにチャレンジするのがオススメです

### ソースコード

- [Glitch を開いてコードを確認できます (02-ar-custom-model.html)](https://glitch.com/edit/#!/lesson-ar-js?path=02-ar-custom-model.html)

### スマホでの動作

- [スマホでリンクを開いて AR の動作を確認できます](https://lesson-ar-js.glitch.me/02-ar-custom-model.html)
- 以下の QRCode をスマホのカメラで写すと AR のオブジェクト（キューブ）が表示されます

<img width="35%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2Fhiro.png?v=1618722159489">

<div>
  <img width="50%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2F02-screen_shot-min.jpg?v=1618755326112">
</div>



## 03: 自分で作成したマーカーを認識させる

- Hello World ではプリセットのマーカーで AR コンテンツを表示していました
- 今回は自分で用意した画像をマーカーにします
- 以下のサイトでマーカーの設定ファイル(.patt)とマーカーの画像ファイルを生成します
  - [AR.js Marker Training](https://jeromeetienne.github.io/AR.js/three.js/examples/marker-training/examples/generator.html)
- マーカーを作るポイントとしては背景は少し灰色にしてシンプルな模様にすると読み取りやすいです
- 色も多くなると読み取りづらいので灰色背景に黒い簡単な図形を描くことをオススメします

### ソースコード

- [Glitch を開いてコードを確認できます (03-ar-custom-marker.html)](https://glitch.com/edit/#!/lesson-ar-js?path=03-ar-custom-marker.html)

### スマホでの動作

- [スマホでリンクを開いて AR の動作を確認できます](https://lesson-ar-js.glitch.me/03-ar-custom-marker.html)
- 今回は自作したマーカーを読み取ります
- 以下の QRCode をスマホのカメラで写すと AR のオブジェクト（キューブ）が表示されます

<img width="35%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2Fpattern-01-qr.png?v=1618674990661">

<div>
  <img width="50%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2F03-screen_shot-min.jpg?v=1618755326449">
</div>

## 04: AR で任意の画像を表示する

- 3D モデルだと自作しづらいので AR で画像を表示させてみます
- カワイイ女の子の画像を AR で飛び出させてみます
- カワイイ女の子の画像の素材は以下のサイトから利用させていただきました
  - http://iwayu2.blog.fc2.com/blog-entry-9.html

### ソースコード

- [Glitch を開いてコードを確認できます (04-ar-display-image.html)](https://glitch.com/edit/#!/lesson-ar-js?path=04-ar-display-image.html)

### スマホでの動作

- [スマホでリンクを開いて AR の動作を確認できます](https://lesson-ar-js.glitch.me/04-ar-display-image.html)
- 今回は自作したマーカーを読み取ります
- 以下の QRCode をスマホのカメラで写すと AR のオブジェクト（かわいいメイドさん）が表示されます

<img width="35%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2Fpattern-01-qr.png?v=1618674990661">

<div>
  <img width="50%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2F04-screen_shot-min.jpg?v=1618755326901">
</div>

## 05: AR で任意の Gif 画像を表示する

- AR.js は aframe を使用しています
- aframe の拡張ライブラリも使用できます
- 今回は Gif 画像を使用するために aframe-gif-shader を使用します
- Gif 画像は以下のサイトから利用させていただきました
  - [Cliply.co](https://cliply.co/clip/animated-sparkles/)

### ソースコード

- [Glitch を開いてコードを確認できます (05-ar-display-gif.html)](https://glitch.com/edit/#!/lesson-ar-js?path=05-ar-display-gif.html)

### スマホでの動作

- [スマホでリンクを開いて AR の動作を確認できます](https://lesson-ar-js.glitch.me/05-ar-display-gif.html)
- 今回は自作したマーカーを読み取ります
- 以下の QRCode をスマホのカメラで写すと AR のオブジェクト（動いてキラキラする何か）が表示されます

<img width="35%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2Fpattern-01-qr.png?v=1618674990661">

<div>
  <img width="50%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2F05-screen_shot-min.jpg?v=1618755326476">
</div>

## 06: ARで複数のマーカーを設定する

- これまでの応用として複数の任意のマーカーを設定します
- それぞれのマーカーから異なるカワイイ女の子の画像を表示します
- マーカーさえ準備できればハーレムにできます

### ソースコード

- [Glitch を開いてコードを確認できます (06-ar-multi-marker.html)](https://glitch.com/edit/#!/lesson-ar-js?path=06-ar-multi-marker.html)

### スマホでの動作

- [スマホでリンクを開いて AR の動作を確認できます](https://lesson-ar-js.glitch.me/06-ar-multi-marker.html)
- 今回は自作したマーカーを読み取ります
- 以下の QRCode をスマホのカメラで写すと AR のオブジェクト（かわいいメイドさん２人）が表示されます

<div>
  <img width="35%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2Fpattern-01-qr.png?v=1618674990661">
  <img width="35%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2Fpattern-02-qr.png?v=1618754463322">
</div>

<div>
  <img width="50%" src="https://cdn.glitch.com/dfc871f2-44bd-4c8a-bb58-5515b3ac1df9%2F06-screen_shot-min.jpg?v=1618755327321">
</div>