---
title: 2020-05-unity-mob-b
jp_title: Unity Mob PJ 「Toilet Paper」
author: 1000170231-maehata
tags:
  - Unity
  - C-Sharp
participant:
  - 000000000-lms
  - 1000140174-tamoco
  - 1000170231-maehata
  - 1000170156-yachi
excerpt: ゲームエンジンのUnityを使いモブプログラミングでミニゲームを作るプロジェクトです
date: 2020-05-11T10:00:00
featuredImage: uploads/unity-mob.png
---

# Unity でペアプロ
2019/05/11に開催。勉強会の参加人数が少なかったのでチームは２つ。その１チームの記録を以下に記載します。

# どんなゲームを作るのか？
トイレットペーパーを全て巻き取ったらクリアというゲームです。操作方法は下にスワイプ？フリック？要するに下に画面を擦る（以降はフリックと記載）のがメインです。

上記以外にも「牛の乳しぼり」も案に挙がっていましたが、なんやかんやで「トイレットペーパー」に決まりました。

ちなみに、前回の「B01_ColorPatter」チームと同じメンバーだったので、前回よりもイイモノを作ろうということをになりました。ちなみに前回の内容はシンプルなゲームだったモノの、若干シンプル過ぎたように思われます。そのため、今回はもうちょっとチャレンジしてみることにしました。

# 早速！素材作成に苦しむ！！

前回、他のチームであった素材難が襲い掛かります。特に今回は「トイレットペーパー」をどうやって回して紙を出していくのか、そこらへんのギミックをどうしていくのかがフワフワしていたため、素材づくりに苦労してしまいました。

正直にいうとこの時点では全然ギミックのイメージが出来てません。こういう動きをしたらソレっぽく見えるのではないかと見せ方についてイロイロ検討した結果、以下のようなカンジになりました。

![図1.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/a801513e-89e8-acd6-2cd3-197d484d0676.png)


- ペーパーの切れ目を上から下に移動させれば何とか見えるかもしれない。
- トイレットペーパー本体を徐々に細くすれば減っているように見えるかもしれない。


# スワイプの処理はどうする？
下に擦るスワイプの操作も重要です。こちらは別働隊（ペアだから一人）が事前にスワイプの処理を調べ、事前に他のプロジェクトで動作検証をしてもらいました。主に素材の準備などで手間取っている時間で目処がついたようで、そのソースを取り込んでみることに。

```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C01_EgdeSlide : MonoBehaviour
{
    float x;
    float y;
    public float StartPos;
    public float EndPos;
    public float movement;
    
    void Update()
    {
        x = Input.mousePosition.x;
        y = Input.mousePosition.y;

        
        if (Input.GetMouseButtonDown(0))
        {
            StartPos = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
            //Debug.Log(StartPos);
        }
        if (Input.GetMouseButtonUp(0))
        {
            EndPos = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
            //Debug.Log(EndPos);
            movement = 0;
        }
        if (StartPos > EndPos)
        {
            movement = (StartPos - EndPos)*0.1f;
        }
        else if (StartPos < EndPos)
        {
            movement = 0;
        }

        // TouchPos = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 10.0f));
        //TouchPos.y = Mathf.Clamp(TouchPos.y, -50f, 3.2f);
        transform.position = new Vector3(transform.position.x, transform.position.y - movement, 10.0f);//Y軸のみ動かせる
    }

}

```

# いろいろと最終調整
各チーム１人ずつのメンバーが先に帰宅することになり、いよいよペアプロも際どい状態に。ちょうど各チーム１人ずつ残ったので、チームを統合する形でペアプロを再開しました。

上記のスワイプの処理は実装済みで切れ目のオブジェクトを上下の動作と連携させました。
残すはトイレットペーパー本体を徐々に細くする処理になります。これが悩んだポイントで「スプライトで表示を切り替えていくのか」「その都度オブジェクトを生成して切り替える」か検討しているうちにどんどん内容が混濁してきます。
結果的には統合されたペアプロで相談しながら整理することで無事に解決できました。

# なんとか完成
![G1Y0rc4dd.gif](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/1c0f2e33-2668-1065-5c79-a952bd4c1272.gif)

切れ目だけ流すだけでロールしているように見えるモンですね。右側のロールが回転すれば更にそれっぽく見えそうです。

# 振り返り
- やっぱり素材作成に苦しんだ
- 次回までに素材だけ準備してきてもよいかも
- このゲームの演出的にはもうちょっと凝りたいので素材を準備してリベンジ？
