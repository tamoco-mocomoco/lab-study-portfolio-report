---
title: 2021-05-22-unity-web-vr
jp_title: Unityで作成するWebVR
author: 1000170231-maehata
tags:
  - Unity
  - C-Sharp
  - VR
participant:
  - 000000000-lms
  - 1000140174-tamoco
  - 1000170231-maehata
  - 1000170156-yachi
  - 1000200280-oono
  - 1000210050-kobayashi
  - 1000210079-nakada
excerpt: Unity2020.3でWebVRのサイトを作成します
date: 2021-05-22T13:00:00
featuredImage: uploads/unity_webgl_webvr.png
---

# 概要

- Unity 2020.3でカンタンなWebVRのサイトを作ります
- Unity-WebXR-Exporterを使用します
- 作る上のパッケージに関する情報や手順などをまとめています
  - 最初はサンプルのデモを動くようにしてホスティングサイトにデプロイします
  - 次にサンプルを元に少しカスタマイズして違うサイトとしてデプロイします
     - 森を散歩するVRをつくります 

# 環境

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/7e970fb1-8120-49d6-6204-b980dcccec38.png)

- Windows 10
- Unity 2020.3.8f1

# 完成物

![Screenshot_2021-05-21-14-51-48-12.jpg](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/7d358380-7f1b-d955-e407-d8925b3126a2.jpeg)

- パッケージにあるサンプル
  - https://unity-webvr-demo.netlify.app/

![Screenshot_2021-05-21-12-52-30-83.jpg](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/c0202a3b-97c2-6349-1ddc-8e449e61052e.jpeg)

- 少しカスタマイズしたモノ
  - https://unity-webvr-custom.netlify.app/

# 使用するパッケージ

## 

- UnityでWebGLでWebVRのコンテンツを作ることができるパッケージです
- Mozillaが提供しているUnity-WebXR-Exporterだと 2020.3で動作しません
  - 2020.2以降でXRDevice.isPresentが削除されておりエラーが表示されます
- 2020.2以降で動作させるためのパッケージが公開されておりそれを利用します
  - https://github.com/De-Panther/unity-webxr-export
     - Mozillaのパッケージでマウスをドラッグして視点を変えることができなくなっています
     - XRDeviceを使用しなくなったことで機能的になくなったモノだと思われます


### XRDevice.isPresentの削除

2020.1だとあまり使われていないので削除されたようです。

#### 削除されたことが上部に記載（Removed）

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/e0034a6a-6a6b-0bcf-26df-7b65535b412a.png)

#### あまり使われていないことが上部に記載（Method group is Obsolete）

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/c0359715-2f31-168e-d1ec-6d59ff30a7f9.png)

# サンプルを元にWebVRをつくる

## 新規のプロジェクトを作成する

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/65cc76c4-db46-d5fe-5166-9d3e1c608b1a.png)

- 新規で3Dのプロジェクトを作成します

## Unity-WebXR-Exporterのパッケージを取り込む

### パッケージのダウンロード

- 以下のGitHubのURLからUnity用のパッケージを取得します
  - https://github.com/De-Panther/unity-webxr-export
- GitHubのページにもありますが以下の２つリンクからパッケージをダウンロードします
  - [WebXR Export] (https://openupm.com/packages/com.de-panther.webxr/)
  - [WebXR Interactions] (https://openupm.com/packages/com.de-panther.webxr-interactions/)
  - 「Get installer.unitypackage」をクリックすることでダウンロードできます

### パッケージのインストール

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/ff999036-2773-a58b-442c-cb0c8358161b.png)

- 「Assets」→「Import Package」→「Custom Package」をクリックします
- 先程ダウンロードしたパッケージを選択します

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/1219678d-925d-1d21-569e-faa31f3e9e04.png)

- 「Import」をクリックします
- もう１つのパッケージも同様の手順でインストールします

## WebGLのテンプレートの作成

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/5b4d6b5a-e229-b356-3e0c-248086f97c23.png)

- 「WebGL」→「Copy WebGLTemplates」をクリックします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/ab9460e3-c13c-f994-2884-279955db0046.png)

- 既存のテンプレートを上書きするため確認のダイアログが表示されるので「Contienue」をクリックします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/15b46606-473f-f44b-b26c-2bdada861e37.png)

- WebGL Templatesが作成されていることを確認します

## Sample Scene をインポート

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/ab0dc92d-6102-c029-2569-c86d3ed3d30e.png)

- 「Window」→「Package Manager」でPackage Managerを開きます

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/36768d81-8664-1654-b5d0-6b5a5581e44c.png)

- 「My Registries」で先程取り込んだパッケージを表示します 

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/246df7ea-d02c-3758-0cee-1be6b082804a.png)

- 「WebXR Interactions」を選択しSamplesの「Import」をクリックします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/d8a9dbea-a591-66f9-3ac2-88f27b5d373a.png)

- 「Sample」が追加されたことを確認します

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/ad521468-4433-3dae-2468-185924638589.png)

- 「Sample」にある「Desert」のシーンファイルをダブルクリックで開きます

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/aa172238-7389-54f5-2ef3-a8a126007a67.png)

- 上部のツールバーの右向きの三角形のアイコンのボタンをクリックしてサンプルのシーンを実行します
- マウスでオブジェクトを掴んで動作することを確認します

## Build Settings の設定

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/58a9336b-422b-1355-9929-66e9cd9a7f2a.png)

- 「File」→「Build Settings」をクリックします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/0e45cb47-ca2d-45d7-75c4-b94f5a23fa81.png)

- 「WebGL」を選択して「Switch Platform」をクリックします
- Platformが切り替わるとファイルが読み込まれます

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/d77fe9e5-9d02-86ea-9d0d-743bc6c89faf.png)

- 「Add Open Scenes」をクリックして現在開いているサンプルのシーンを追加します
- 「Player Settings」をクリックします

## Player Settings の設定

### XR Plug-in Management

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/92f46863-df21-b497-8da8-3e2e0bd4dd70.png)

- 有効になっていない場合は画像のとおり設定します

### Player の設定

WebGL settingsのタブを開きます

#### Resolution and Presentation

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/b4541dcc-7047-9a87-14e4-d4aa53b3eaa8.png)

- 前の手順でCopy WebGLTemplatesしたモノが表示されています
- 「WebXRFullView2020」をクリックします

#### Publishing Settings

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/5073cb17-ccd2-387a-f301-e35d0bed5b7c.png)

- 「Decompression Fallback」にチェックを入れます
- Player Settingsが完了したので画面を閉じます

## サンプルをビルドする

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/0901b438-2c36-d338-c683-10c61b6b88e1.png)


- 先程の手順から「Build Settings」の画面が表示されているはずです
  - 開いていない場合は上記の手順から「Build Settings」を開きます
- 「Build」をクリックします
- 任意のフォルダを選択してファイルを出力します

## Buildしたファイルをデプロイする

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/f198283c-5a17-4690-be53-82052608d325.png)

- 先程の出力したフォルダを確認すると「index.html」などが格納されているはずです
- それらのファイルをウェブサーバーに展開することでブラウザでWebVRとして動作します
- 今回はNetlifyというホスティングサービスにデプロイしました
  - https://unity-webvr-demo.netlify.app/


# 少しカスタマイズする

- Asset StoreにあるEnvironmentを使ってお散歩するようなVRコンテンツを作ります

## EnvironmentのAssetを取り込む

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/08385310-fd5a-23ab-da25-579dbb656223.png)

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/2379df08-6e33-75f5-2ef1-80b41d9ac5e1.png)


- 今回は森のAssetを使用します
  - https://assetstore.unity.com/packages/3d/environments/fantasy/glowing-forest-79686
- 「Add to My Assets」をクリックして現在開いているプロジェクトに取り込みます
- 「Package」の画面が表示されたら「Import」をクリックして取り込みます


![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/73a2bcd8-75ad-b1fb-3e93-dc33c79dd09a.png)

- Importされたことを確認します
  - 「SkythianCat」というフォルダが追加されています

## WebVRのカメラを取り込んだアセットのシーンにコピーする

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/e41ce2cf-558f-d9f8-e90b-ca4817431757.png)


- WebXR ExporterのDesertのシーンにあるWebVR用のカメラをコピーします
  - 「WebXRCameraSet」を選択して「Ctrl」+「C」でコピーします

- 取り込んだアセットのDEMOのシーンを開きます
  - 「SkythianCat」→「Glowing_Forest」→「Demo_scene01」をダブルクリックします
- コピーしたカメラを取り込んだアセットのDEMOのシーンに貼り付けます
  - 「Ctrl」+「V」で貼り付けます 
- 既存のカメラである「GROUP_Cameras」を削除します
  - 「GROUP_Cameras」を「Delete」キーで削除します 

### WebVRのカメラの位置を調整する

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/723a350e-af67-5b64-9cf6-f158d9c99704.png)

- 「WebXRCameraSet」を選択して位置調整をします
  - TransformのPosition以下のとおりに設定します
    - X:47 Y:150 Z:95
  - 実行してカメラが森の中にいることを確認します


## カメラをコントロールするスクリプトを作成する

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/8c7f328a-9c4f-d9e7-6434-eb18239c0e3c.png)

- 今回は「Assets」フォルダの直下にカメラをコントロールするスクリプトを作成します
  - 「CameraController.cs」というファイルを作成します
  - コードは以下の内容をそのままコピーしてください
- 作成したスクリプトを「WebXRCameraSet」にアタッチします
  - アタッチ後、Publicの変数で表示されたCam Followerにもアタッチします
    - 「WebXRCameraSet」→「Cameras」→「CameraFollower」をアタッチします
- 上記の設定が完了すると以下のように動作します
  - カメラが向いている方向に移動します
  - カメラの向きは画面中央よりクリックした位置で回転します

```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject camFollower;
    public float moveSpeed = 1;
    public float camRotateSpeed = 1;
    int screenW = Screen.width;
    void FixedUpdate()
    {
        ClickMove();
        DirectionMove();
    }

    void DirectionMove()
    {
        // VRだとFollowerのオブジェクトを参照する
        Vector3 velocity = camFollower.transform.rotation * new Vector3(0, 0, moveSpeed);
        gameObject.transform.position += velocity * Time.deltaTime;
    }

    void ClickMove()
    {
        // 左ボタンクリック
        if (Input.GetMouseButton(0))
        {
            float mousePositionX = Input.mousePosition.x;
            if (screenW / 2 > mousePositionX)
            {
                // 画面の中央より左側をクリックするとカメラが左に回転
                gameObject.transform.Rotate(new Vector3(0, camRotateSpeed * -1, 0));
            } else {
                // 画面の中央より右側をクリックするとカメラが右に回転
                gameObject.transform.Rotate(new Vector3(0, camRotateSpeed, 0));
            }
        }
    }
}

```

## Rigidbody・Colliderを設定する

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/07323918-88ca-d8f6-9648-d023cb67a9a4.png)

- 今は浮遊している状態なので重力と物理的な機能をつけて地面を歩くような形します
- それぞれのコンポネントを追加します
  - 「WebXRCameraSet」を選択し以下のコンポネントを追加します
    - 「Physics」→「Rididbody」
    - 「Physics」→「Box Collider」
- カメラの位置を調整します
  - このままだと床をすり抜けてしまうので若干高くします 
  - カメラのposition　　X:47 Y:152 Z:90
- Box Colliderを調整します
  - 狭いところが通れないのでサイズを小さくします
  - Sizeを以下の設定にします
  - Box Collider Size　　X:0.5 Y:0.3 Z:0.5
  
## Sceneを追加してBuildする

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/174086fa-3ca5-b731-1b00-f0955bcd6e18.png)

- 「File」→「Build Settings」をクリックします
- 「Add Open Scenes」で現在開いているシーンを追加します
- 最初に追加したシーンは選択して「Delete」キーで削除します
- 「Build」で出力します
- 以下の場所にデプロイしました
  - https://unity-webvr-custom.netlify.app/

# ふりかえり

- Asset Storeにある「Unity-WebXR-Exporter」だと2020.3では動作しない
- 有志で作成しているパッケージをインストールすることでカンタンにつくれる
- なぜ動作しないか原因を知ることができた
  - 2020.2以降でXRDevice.isPresentが削除されていたため
- 一度作り方を覚えればいろいろ工夫してVRのコンテンツを作れそう
