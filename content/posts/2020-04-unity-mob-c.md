---
title: 2020-04-unity-mob-c
jp_title: Unity Mob PJ 「Color Pattern」
author: 1000170231-maehata
tags:
  - Unity
  - C-Sharp
participant:
  - 000000000-lms
  - 1000140174-tamoco
  - 1000170231-maehata
  - 1000170156-yachi
excerpt: ゲームエンジンのUnityを使いモブプログラミングでミニゲームを作るプロジェクトです
date: 2020-04-20T10:00:00
featuredImage: uploads/unity-mob.png
---

# Unity でモブプロしました

4月20日、Unityでモブプログラミングを行った記録です。シンプルなミニゲームを作りました。「B01_ColorPattern」チームの内容になります。

# 先月度のことを振り返る

先月に一度実験的にモブプロをやってみたのですがシンプルにするのが重要に思いました。最初はいろいろやりたいこととか出てくるのですが操作担当者のレベルも違ってくるので難しいことをやろうとすると結構厳しいことになります。今回は２回目となりますが規模は大きくせずシンプルなゲームを目指します。

# ゲーム内容

ゲーム素材も用意する必要があるため出来るだけシンプルでわかりやすいモノにしました。

- 画面に円形のボタンを１２個表示
- それぞれ「赤色：４」「青色：４」「緑色：４」
- 同じ色を３つタッチするとクリア
- ３秒以内に同じ色を３つタッチしないとゲームオーバー

実はこのゲームで使用されている素材は白の円形の画像が１つだけになります。他のチームでは素材づくりで苦労したようでシンプルにして良かったです。ただ、オリジナリティなどを出そうとすると素材は必要になったりするので、そこは今後の課題になってくると思います。

【使用した素材】

<img src="https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/559d3c5f-3db8-088c-f852-f5ff3a37fd89.png" width="20%">

わかりやすくするために黒枠を追加しています。実際は黒枠はありません。


# 実装内容

全ての経緯を記載すると煩雑になるのでピックアップして記載します。最も重要な実装ポイントは同じ色だけを３つタッチした場合のみクリアするということで、その部分を抜粋してコードを記載します。

```
    private int ClickCount = 0;
    private int point = 0;
    // Start is called before the first frame update
    void Start () {
        point = 0;
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetMouseButtonDown (0)) {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
            Collider2D targetCollider = Physics2D.OverlapPoint (touchPos);
            //タグによる検知をやめてnameの中に文字が含まれているかで検知
            //if (targetCollider.gameObject.tag == "B01_Red")
            if (targetCollider.gameObject.name.Contains ("Red")) {
                Destroy (targetCollider.gameObject);
                Debug.Log ("Tag=Red");
                point += 1;
                ClickCount += 1;
            } else if (targetCollider.gameObject.name.Contains ("Green")) {
                Destroy (targetCollider.gameObject);
                Debug.Log ("Tag=Green");
                point += 10;
                ClickCount += 1;
            } else if (targetCollider.gameObject.name.Contains ("Blue")) {
                Destroy (targetCollider.gameObject);
                Debug.Log ("Tag=Blue");
                point += 100;
                ClickCount += 1;
            }
        }
        if (ClickCount == 3) {
            Debug.Log ("GAME SET");

            if (point == 3) //赤のカウント
            {
                Debug.Log ("ゲームクリア！");
                SceneManager.LoadScene ("B01_GameCrear");
            } else if (point == 30) //緑のカウント
            {
                Debug.Log ("ゲームクリア！");
                SceneManager.LoadScene ("B01_GameCrear");
            } else if (point == 300) //青のカウント
            {
                Debug.Log ("ゲームクリア！");
                SceneManager.LoadScene ("B01_GameCrear");
            } else {
                Debug.Log ("ゲームオーバー");
                SceneManager.LoadScene ("B01_GameOver");
            }
        };

    }
```

同じ色を判別するというのが古典的な方法を選択しました。赤は数値の一桁目、緑は数値の二桁目、青は数値の三桁目を計算対象にして判別するようにしています。他にもやり方はいろいろとありそうですが、モブプロで個人個人ができる実装を行った結果だと思います。

その他に気になったことしては「Bo1_timelimit」と「0」が「o」になっていて明らかにタイポがありました。ただそれも操作担当者が切り替わっていくうちに放置されてしまったので、誰かが引き受けて修正しないとダメでした。

【タイポを確認】
<img src="https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/08b29023-b8ef-08f4-c26a-4d067fc45a4b.png" width="70%">



# 実際のゲーム画面

■タイトル画面
<img src="https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/b1934379-d127-b1ea-2d5c-a3951f17eabb.png" width="50%">


■ゲーム画面
<img src="https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/5a6e5b16-a1aa-d305-fd9c-48662fbee5ad.png" width="50%">
タイムは 3 秒から開始で同じ色を 3 つタップするとクリア。

■結果画面（クリア）

<img src="https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/e4ff9295-31d8-b2b5-67be-56dbc191e808.png" width="50%">

■結果画面（タイムオーバー）

<img src="https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/6ff0be63-e57e-8e04-b9b6-ed566c3938ed.png" width="50%">

マウスポインタが入ってます・・・。


# 今回の内容を振り返って

- シンプルにすると進めやすい
- どうしても妥協した部分がある
- できるだけ見識者が助言してよくしていきたい（モブプロのイイところを出したい）
- タイトル画面や結果画面の UI に手を出したかった
