---
title: 2020-12_svelte-js
jp_title: Svelte.jsによるゲーム開発
author: 000000000-lms
tags:
  - Frontend
  - JavaScript
  - Svelte
participant:
  - 000000000-lms
  - 1000140174-tamoco
  - 1000170231-maehata
excerpt: Svelte.jsの基礎から学びおみくじアプリを作成します
date: 2020-12-19T10:00:00
featuredImage: uploads/svelte-js.png
---

# 概要
- Svelte.jsの基礎から学習しました
- 既存のフロントエンドのフレームワークとの比較をしました
- [【example】](https://svelte.dev/examples)を元に学習を進めREPLによるパッケージダウンロードでおみくじアプリを作成しました

# svelte.jsについて

単一ファイルコンポーネントである`.svelte`ファイルの中にコンポーネントとしてHTML, CSS, JavaScript相当のコードを記述します。ReactやVue.jsのようにコンポーネントを組み合わせてWebアプリケーションを構築します。

# 基礎
- Hello Worldから始めどのように描画されるか説明しました
- JavaScriptの変数はView側で直感的に使用できます
- Vue.jsと比較して受け取り専用のプロパティのような役割に分かれていないのが特徴です

# 応用
- コンポネントごとに渡す場合はそれぞれに変数を渡し合うことができます
- ただし各コンポネントを小さくと管理が煩雑になるケースもあります
- 共通で変数を利用する場合はstoreを利用することで管理しやすくなります

# 成果物
- [【おみくじアプリ】](https://svelte-de-tamoco-omikuji.netlify.app/)
