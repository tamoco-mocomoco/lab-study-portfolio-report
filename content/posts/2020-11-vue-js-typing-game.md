---
title: 2020-11_vue-js-typing-game
jp_title: Vue.jsでタイピングゲーム開発
author: 000000000-lms
tags:
  - Frontend
  - JavaScript
  - Vue
  - Bulma
participant:
  - 000000000-lms
  - 1000140174-tamoco
  - 1000140178-yasu
  - 1000170231-maehata

excerpt: Vue.jsの基礎から学びタイピングゲームを作成します
date: 2020-11-21T10:00:00
featuredImage: uploads/vue-typing-game.png
---

# 概要
- Vue.jsによるタイピングゲームの作成しました
- 初心者でも簡単に作れるように環境構築も省略しました
- コード量も100行程度で作りきることが可能です
- CDNによるフレームワークの読み込みからHTMLを基礎を学びました

# HTMLページだけで動作するコード

```
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>レッツタイピング</title>
    <link rel='stylesheet' href='https://unpkg.com/bulma@0.9.0/css/bulma.min.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
</head>

<body>
    <div id="app">
        <section class="hero is-info is-fullheight">
            <div class="hero-body">
                <div class="container">
                    <nav class="level">
                        <div class="level-left">
                            <div class="tile is-parent is-vertical">
                                <h2 class="tile is-child is-size-1 title has-text-left">
                                    Hi-Score: {{hiScore}}
                                </h2>
                                <h2 class="tile is-child is-size-1 title has-text-left animate__animated animate__faster"
                                    v-bind:class="{animate__bounce: isScoreAnime}">
                                    Score: {{score}}
                                </h2>
                            </div>
                        </div>
                        <div class="level-right">
                            <div class="tile is-parent is-vertical mt-4">
                                <h2 class="tile is-child is-size-1 title has-text-left">
                                    Time: {{time}}
                                </h2>
                            </div>
                        </div>
                    </nav>
                    <h1 class="title has-text-centered is-size-1 pb-4">
                        {{targetText}}
                    </h1>
                    <div class="subtitle has-text-centered">
                        <input class="input" style=" width: 40%;" v-model="inputText" @input="InputText" type="text"
                            placeholder="Let's Type !" v-bind:disabled="endGame" autofocus>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        var app = new Vue({
            el: "#app",
            data: {
                textList: ["こんにちは", "こんばんは", "ぷにぷにワダオ", "鳥貴族", "AWS", "アプリケーション", "ポップコーン", "冬休み"],
                inputText: "",
                targetText: "",
                score: 0,
                hiScore: 0,
                time: 10,
                intervalTimeObj: null,
                isScoreAnime: false,
                endGame: false
            },
            methods: {
                InputText() {
                    if (this.targetText == this.inputText) {
                        this.isScoreAnime = true;
                        this.inputText = "";
                        this.score++;
                        this.ChangeTarget();
                    }
                },
                ChangeTarget() {
                    this.targetText = this.textList[Math.floor(Math.random() * this.textList.length)]
                    setTimeout(() => { this.isScoreAnime = false }, 1000);
                },
                EndGame() {
                    this.endGame = true;
                    this.targetText = "=== ゲーム終了 ===";
                    this.inputText = "";
                    clearInterval(this.intervalTimeObj);
                    if (this.score > this.hiScore) {
                        localStorage.setItem('hiScore', this.score);
                        this.hiScore = this.score;
                    }
                }
            },
            created: function () {
                alert("表示された文字を入力するタイピングゲームです。\n\nボタンを押すとスタートします。")
                this.hiScore = localStorage.getItem("hiScore") ? localStorage.getItem("hiScore") : 0;
                this.ChangeTarget();
                this.intervalTimeObj = setInterval(() => { this.time-- }, 1000);
            },
            watch: {
                time: function () {
                    if (this.time <= 0) {
                        this.EndGame();
                    }
                }
            }
        })
    </script>
</body>

</html>
```


# 実際の成果物

- [【タイピングゲーム】](https://tamoco-mocomoco.gitlab.io/vue-de-typing-game/)

# 補足事項

- これらの講座以外に準備され講座内容は受講者によって選択されました
- [準備していた講座内容](https://docs.google.com/presentation/d/1SgI1iyJPx_wnfJ3-AC4gZERzSHvFEsVQyA4fseElyJs/edit#slide=id.p)
