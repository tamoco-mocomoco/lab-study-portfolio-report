---
title: 2021-05-22-unity-install
jp_title: Unityのインストール
author: 1000170231-maehata
tags:
  - Unity
participant:
  - 000000000-lms
  - 1000140174-tamoco
  - 1000170231-maehata
  - 1000170156-yachi
  - 1000200280-oono
  - 1000210050-kobayashi
  - 1000210079-nakada
excerpt: ゲームエンジンのUnityを使いモブプログラミングでミニゲームを作るプロジェクトです
date: 2021-05-22T10:00:00
featuredImage: uploads/unity.png
---

# 概要

- 久しぶりにUnityを新規でインストールしました
- カンタンなモノですが2020.3のインストール手順としてまとめています
- 今回の手順はWindows10向けの内容になります
- あくまで投稿時点のスクリーンショットですのでご参考までに・・・

# 環境

- Windows 10

# インストール手順

## Unity Hub のダウンロード

- 以下のURLからUnity Hubをダウンロードします
- https://store.unity.com/ja#plans-individual

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/5438535a-48c3-087e-bc83-d2c44c23e096.png)

- 以下の利用規約から外れていないか確認します
  - 収入ならびに資金調達(自己資金を含む)の過去12ヶ月の合計が年間 10 万ドルを超えない場合、 Personal を使用できます。
  - もし規約から外れている場合はTeamsタブから有料のモノを選択しましょう
- Personalの「はじめる」をクリックしてページを移動します

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/44369bc8-0343-8abf-190c-d1485c7440b9.png)

- 新規ユーザーの「始めよう」をクリックします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/d38951d9-3a05-689c-72b5-0431ca852b1e.png)

- 「同意してダウンロードする」をクリックして「UnityHubSetup」をダウンロードします

## Unity Hub のインストール

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/9b7703d1-a98c-2d17-c508-e6d814b0daf9.png)

- 同意するをクリックします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/92a45138-13c2-d689-3b65-3c67c438509d.png)

- インストールをクリックします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/e677c7a4-9090-29b0-b3cc-3a0a66ea4b32.png)

- インストールが完了するのを待ちます

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/f4b9e8e1-f152-a990-7fd9-008b17cf3d81.png)

- Unity Hubを実行にチェックが入っていることを確認し「完了」をクリックします

## Unity Hub から Unityをインストール

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/9240e9f4-2e36-91c9-23e7-5169ffbec326.png)

- 直前の手順からUnity Hubが起動します
- セキュリティの重要な警告が表示されるので「アクセスを許可する」をクリックします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/3e3f9b41-8b4d-6d71-09f8-1b7028ba5cf4.png)

- 「インストール」をクリックします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/5ac50f89-3df1-8f71-2f96-8911e8e52174.png)

- 「すべてのCookieを受け入れる」をクリックします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/217cdbf1-284d-f2bd-6c57-a6c97bf0d1e4.png)

### Unity ID の作成

- Unity IDを持っていない人はこの画面からUnity IDを作成します
- 以下の２つの方法から好きな方法を選択してください
  - ユーザーネーム、メールアドレス、パスワードを入力しチェックマークをすべて入れてUnity IDを作成をクリックする 
  - 最下部にあるGoogleやFacebook、Appleのアカウントと連携してUnity IDを作成する
- すでにUnity IDを持っている方は「すでにUnity IDを所有している」をクリックしてログインします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/ac52ac56-226f-891c-b1a0-aded01db1b55.png)

- launcherが起動した場合は「OK」をクリックします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/2626dfe7-314b-2d50-fd54-c1fb49e28f8e.png)

- この画面ではゲームのサンプルゲームを新しいプロジェクトとして作成できます
- 今回はサンプルゲームではなく3Dゲームをまっさらの状態で作る「3D」を選択し「次へ」をクリックします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/d99d558f-43be-97ca-744a-8c5ac19f4b86.png)

- Unityのダウンロードを待ちます

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/e90b2a82-5348-025e-2416-c7084bd70d1e.png)

- ダウンロードが完了したら「Unityを起動」をクリックします

## Unityの起動

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/d85d8cea-ef71-f59c-8d0c-87cc08e7282a.png)

- セキュリティの重要な警告が表示されるので「アクセスを許可する」をクリックします

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/03251047-e0db-8036-7612-a72b7f85596a.png)

- Unityが起動します

## 補足

### Unity Hubとは

- Unity Hub は、Unity のプロジェクトとあらゆるバージョンのインストーラーの検索、ダウンロード、管理などを効率化するスタンドアロンアプリケーションです
- すでにマシンにインストールされているエディターのバージョンを Hub に手動で追加することもできます
- いろんなバージョンを持つことで作成したゲームのバージョンアップを任意のタイミングで実行できます

![image.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/412322/de775970-d1ea-ca01-cde0-e37d14970e82.png)


### Unity のバージョンについて

- １つのバージョンだけあれば開発できるのでは？という疑問があるかもしれません
- バージョンがあがると新しい機能が追加されたりします
- 今まで使っていた機能の仕様が変わって動かなくなることもあります
- 機能の変更だけならよいのですが機能が消えることもあります
- １つのバージョンで強制的にアップデートされると上記の理由で作成したゲームがメンテナンスできなくなる可能性があります
- 複数のバージョンを持つことでアップデートするタイミングを先送りにして任意で実行できます
