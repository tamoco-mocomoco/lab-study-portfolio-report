---
title: 2020-10_getting-things-done
jp_title: Getting Things Done
author: 000000000-lms
tags:
  - Agile
participant:
  - 000000000-lms
  - 1000140174-tamoco
  - 1000140178-yasu
  - 1000170231-maehata
  
excerpt: ヒアリングからやりたいことを抽出し、それらの実現に向けて進め方を考えます
date: 2020-10-17T10:00:00
featuredImage: uploads/latest-manufacturing-basic.png
---

# 概要

- 受講者のやりたいことを付箋ツールなどで洗い出します
- それらの情報を元に開発の方法やツールなどを紹介します
- 講座内容は受講者の情報を元にインタラクティブに進めます

# 対象のエンジニア

- 何かアプリをつくってみたいけどどう進めていくかわからない人
- 自分の技術を伸ばすことにチャレンジしたいけど迷っている人
- 現在成果物を作っているけど悩んでいて状況を整理したい人

# 講座を受講することで得れること

- 成果物の精査：作りたいアプリ（成果物）に必要な技術や手法
- エンジニアの指針：エンジニアとして成長する上で必要なこと

# 講座の進め方（付箋を使った方法）について

- 付箋を使った方法については以下のQiita記事を参照
  - [【開発未経験のエンジニアでも開発業務に就くことができるプロジェクト】](https://qiita.com/tamoco/items/cf657ec4d74ccbf4d493)
  - [【やりたいことを実現するためのワークショップをやってみた】](https://qiita.com/tamoco/items/04380288190c0c9b31ba)


# 成果物

- [【ヒアリングと相談結果のドキュメント（Google ドキュメントを開きます）】](https://docs.google.com/document/d/16SzyHTZHVk8eu9rQnPLXPTm4RG604UaQsE3pcO5gGOs/edit)
- 以下は受講者によるドラフトの結果

<img src="/study-report/2020-10-01.png" alt="受講者によるドラフト">


