---
title: 2020-11_mechanical-design
jp_title: 自動車の基礎知識
author: mechanical-design
tags:
  - Design
  - Beginner
excerpt: 自動車の知識を中心に機械設計の知識を共有
date: 2020-11-21T10:00:05
featuredImage: uploads/car.png
---

# 概要

- 自動車の基礎的な知識を学べる講座です
- 機械系設計者として働く中で必要な自動車の知識を学きます
- 様々な部品から成立している自動車に対して知識を共有
- 普段の就業先でもコミュニケーションの一つとして利用してもらえる意図もある