---
title: 2020-10_mechanical-design
jp_title: 機械系設計者の初級段階として基本的な考え方
author: mechanical-design
tags:
  - Design
  - Beginner
excerpt: 機械系設計者の初級段階として基本的な考え方や、社会人として必要不可欠なことについて説明します
date: 2020-10-17T10:05:00
featuredImage: uploads/mechanical-design-basic.png
---

# 概要

- 機械系設計者の初級段階として基本的な考え方
- 社会人として必要不可欠なことについて説明
- 自動車の基礎的な知識を学べる講座を行いました
- 機械系設計者として働く中で自動車の知識を共有
- 皆さんのエンジニアとしての能力向上を図りました