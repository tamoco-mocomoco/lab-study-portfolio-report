---
id: vb-for-app
title: 'VBA勉強会'
link: 'github.com/chad-collins'
image: 'uploads/author-images/vb-for-app.png'
email: 'contact@chadcollins.net'
blurb: 'VBAを学習するための勉強会'
---

# Bio

VBAを集中的に学習する勉強会。