---
id: mechanical-design
title: 'Mechanic(機械設計勉強会)'
link: 'github.com/chad-collins'
image: 'uploads/author-images/mechanical-design.png'
email: 'kikai@test.com'
blurb: '機械設計を中心とした勉強会'
---

# Bio

機械系設計者の初級段階として基本的な考え方や
社会人として必要不可欠なことについて説明したいと思います。
