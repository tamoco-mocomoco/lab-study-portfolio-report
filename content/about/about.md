---
title: About
excerpt: このページ関する説明
featuredImage: uploads/latest-manufacturing-basic.png
tag: [
    "Agile",
    "Android",
    "Backend",
    "Beginner",
    "Bulma",
    "C-Sharp",
    "CAD",
    "CSS",
    "Container",
    "Database",
    "Design",
    "Machine",
    "Environment",
    "Flask",
    "Frontend",
    "Git",
    "GitHub",
    "Gradle",
    "HTML",
    "Java",
    "JavaScript",
    "jQuery",
    "Linux",
    "Mac",
    "Objective-C",
    "Progressive Web Apps",
    "PHP",
    "Python",
    "PostgresSQL",
    "Qiita",
    "Ruby",
    "Spring",
    "Svelte",
    "TypeScript",
    "VBA",
    "Vim",
    "iOS",
    "iPhone",
    "Unity",
    "Vue"
 ]
---

# 目次

### [投稿者の方はこちら](#投稿者)

<a href="#投稿者">
<img src="/about/pending-approval.png" width="20%" alt="pending-approval">
</a>

### [閲覧者の方はこちら](#閲覧者)

<a href="#閲覧者">
<img src="/about/followers.png" width="20%" alt="followers">
</a>


## 投稿者

<img src="/about/post.png" width="50%" alt="post">

### 概要

- これまでに開催された勉強会の活動内容を記録したサイトです
- ただ活動内容を記録するだけではなく参加した講座をポートフォリオのように使用できます
- また自分が勉強した内容を投稿することで自分の実績として記録することもできます。

### 投稿画面

- 以下のリンクから投稿することができます
- 投稿画面は開発中なためリポジトリをクローンしてコーディングするやり方をオススメします
- コードのプッシュする権限については管理者に付与してもらうようお願いします

- [【投稿者画面のリンク】](/admin)


## 閲覧者

<img src="/about/bookmarks.png" width="50%" alt="bookmarks">

### 概要

- このサイトは各エンジニアが勉強した実績が記録されています
- どのエンジニアがどういった勉強をしてどういったスキルを持っているか把握することもできます
- 一人のエンジニアの履歴からタグなどを利用して他の関連するエンジニアを探し出せます

### ポートフォリオとして使う例

- ここでは一人のエンジニアの活動履歴をポートフォリオのように使ってみます
- 「Y.K」というエンジニアを対象にした場合は以下のURLのページだけで大体のスキルや活動履歴がわかります
- 各エンジニアのページのURLを取得するだけでポートフォリオとして機能します

[Y.Kのポートフォリオとして使う場合のページ](../../author-study/1000140178-yasu/)
