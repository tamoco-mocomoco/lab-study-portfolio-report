# 概要

- これまでに開催された勉強会の活動内容を記録したサイトです
- ただ活動内容を記録するだけではなく参加した講座をポートフォリオのように使用できます
- また自分が勉強した内容を投稿することで自分の実績として記録します

## 投稿者の新規登録・再編集する

### 専用のエディタの利用

- `editor/Author_Editor.html`を開いて登録や再編集を行えます
- 使い方は`editor/Author_Readme.md`を確認してください

### 直接投稿者のファイルを作成する

- `content/authors/`の配下にユーザーのファイルを作成します
- `0000000000-lms.md`のファイルを参考に作成してください
- `id`と`title`は同じで以下の命名規則を守ってください
  - 「社員番号」-「ご自身のアカウント名」
  - 例：1000140174-tamoco

## 講座や勉強した内容を投稿する

- `content/posts/`の配下に講座や勉強した内容のファイルを作成します

# Gridsome の導入

## About

Gridsome Starter with Netlify CMS & Buefy

### See it Live

https://gridsome-starter.chadcollins.net

## Features

- Gridsome
- Netlify CMS
- Buefy (a Vue Javascript layer for Bulma)
- SCSS
- Markdown
- Tags
- Author Pages
- Blog
- About Page
- GraphQL

## Install

### 1. Install the Gridsome CLI tool

`npm install --global @gridsome/cli`

### 2. Create a Gridsome project

1. `gridsome create my-blog`https://github.com/chad-collins/gridsome-bulma-netlifycms to install gridsome with this starter
2. `cd my-blog` to open the folder
3. `gridsome develop` to start a local dev server at `http://localhost:8080`

4. Enable Github Oauth

5. Enable netlify identity

6. Change Repo info at top of admin/config.yml to your own repo.
